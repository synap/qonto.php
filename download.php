<?php
/**
 * Quick and dirty attachment downloader for Qonto Business Bank accounts
 *
 * @author Charles-Édouard Coste <cc@synap.fr>
 */

require 'vendor/autoload.php';

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
$dotenv->loadEnv(__DIR__.'/.env');

$login = $_ENV['QONTO_LOGIN'];
$password = $_ENV['QONTO_SECRETKEY'];
$host = $_ENV['QONTO_HOST'];
$timezone = $_ENV['QONTO_TIMEZONE'];

$client = HttpClient::create([
    'headers' => [
        'Authorization' => "{$login}:{$password}"
    ]
]);

$response = $client->request('GET', "{$host}/organizations/{$login}");

$current_page = 1;
$timezone = new DateTimeZone($timezone);

foreach($response->toArray()['organization']['bank_accounts'] as $account) {

    do {
        $transactions = $client->request('GET', "{$host}/transactions", [
            'query' => [
                'slug' => $account['slug'],
                'current_page' => $current_page
            ]
        ])->toArray();

        $meta = $transactions['meta'];
        $transactions = $transactions['transactions'];

        foreach($transactions as $transaction) {
            foreach($transaction['attachment_ids'] as $attachment) {

                $date = new DateTime($transaction['settled_at']);
                $date->setTimezone($timezone);

                $attachment = $client->request('GET', "{$host}/attachments/{$attachment}")->toArray();

                copy($attachment['attachment']['url'], 'attachments/'. $date->format('Y-m-d_His').'-'.$attachment['attachment']['file_name']);
            }
        }
        $current_page += 1;
    } while($meta['next_page']);
}
