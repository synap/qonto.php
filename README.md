Qonto.php
=========

This is a quick and dirty script to download attachments from my Qonto bank account.

How to use
----------

This project uses PHP >=7.0 and composer.

    $ composer install

Then copy `.env.dist` to a new `.env` file, and edit `.env` with your own parameters.

Then…

    $ php download.php

This will download attachment files into `attachments` folder and name files by using
settled dates and original filename.

Example: `attachments/2019-02-23_120394-my-original-file-name.png`
